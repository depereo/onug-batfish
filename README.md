# ONUG November 2019: Batfish, Briefly.

## Set up Batfish
Batfish runs a service in a container; you utilize a local client to interact with this.
```
$ docker pull batfish/allinone
$ docker run -v batfish-data:/data -p 8888:8888 -p 9997:9997 -p 9996:9996 batfish/allinone
```


## Install pybatfish for interactions
It's recommended to do this in a venv for tidiness.
```
$ python3 -m pip install --user --upgrade git+https://github.com/batfish/pybatfish.git
```

## Run a test!
In this case, we'll check leaves can all route to each other.

```
$ python3

from pybatfish.client.commands import *
from pybatfish.question.question import load_questions
from pybatfish.question import bfq
from termcolor import colored
bf_init_snapshot("./snapshot/")
load_questions()

def check_routes(isFailed, dstNode):
    #get fabric IPs
    ips = bfq.ipOwners().answer().frame()
    #pare down to loopbacks
    loopbacks = ips[(ips['Interface'] == 'Loopback0') & (ips['Active'])]
    #get dstNode IP (and add /32 mask)
    dstIP = [x for x in loopbacks[loopbacks['Node'] == dstNode]['IP']][0] + '/32'
    #create list of leaves and remove destination test node
    leaves = set(loopbacks[loopbacks['Node'].str.contains('l')]['Node'])
    leaves.remove(dstNode)
    #get fabric routes
    fabric_routes = bfq.routes(rib='bgp').answer()
    #check per-leaf bgp rib, check for test node IP
    for leaf in leaves:
            bgprib = [row['Network'] for row in fabric_routes['answerElements'][0]['rows']
                                        if row['Node']['name'] == leaf]
            if dstIP in bgprib:
                print(colored(f"route from {leaf} to {dstIP} present", 'green'))
            else:
                isFailed = True
                print(colored(f"{leaf} bgp rib doesn't include {dstIP} from leaf {dstNode}", 'red'))
                print(leaf, bgprib)
    return isFailed


assert not check_routes(False, 'test-c99-f0103-lyc01a'), "BGP not correctly propogating leaf to leaf routes"
```

Future changes proceed, comfortable in the knowledge that whatever you do manage to break won't include leaf to leaf routing.

