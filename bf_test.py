#!/usr/bin/env python3

from pybatfish.client.commands import *
from pybatfish.question.question import load_questions, list_questions
from pybatfish.question import bfq
from termcolor import colored

def test_controlplane(isFailed):
    # Define a list of Spine switches
    spines = set(bfq.nodeProperties(nodes='sc.*').answer().frame()['Node'])
    print("Progress: Analyzing control plane properties")
    # Get all BGP session status for leaf nodes
    #bgp = bfq.bgpSessionStatus(nodes='leaf.*', includeEstablishedCount=True).answer().frame()
    bgp = bfq.bgpSessionStatus(nodes="l.*", status='ESTABLISHED').answer().frame()
    # All leaves should have at least one peering with each spine
    violators = bgp.groupby('Node').filter(lambda x: set(x['Remote_Node']).difference(spines) != set([]))
    if len(violators) > 0:
        print(colored("Found leaves that do not have at least one peering to each spine", 'red'))
        print(violators[['Node', 'Remote_Node']])
        isFailed = True
    else:
        print(colored("All leaves have at least one peering with each spine", 'green'))
    # All leaves should only peer with spines
    non_spines = bgp[~bgp['Remote_Node'].str.contains('sc', na=False)]
    if len(non_spines) > 0:
        print(colored("Leaves do not only peer with spines", 'red'))
        print(non_spines[['Node', 'Remote_Node']])
        isFailed = True
    else:
        print(colored("Leaves only peer with spines", 'green'))
    return isFailed


def check_routes(isFailed, dstNode):
    #get fabric IPs
    ips = bfq.ipOwners().answer().frame()
    #pare down to loopbacks
    loopbacks = ips[(ips['Interface'] == 'Loopback0') & (ips['Active'])]
    #get dstNode IP (and add /32 mask)
    dstIP = [x for x in loopbacks[loopbacks['Node'] == dstNode]['IP']][0] + '/32'
    #create list of leaves and remove destination test node
    leaves = set(loopbacks[loopbacks['Node'].str.contains('l')]['Node'])
    leaves.remove(dstNode)
    #get fabric routes
    fabric_routes = bfq.routes(rib='bgp').answer()
    #check per-leaf bgp rib, check for test node IP
    for leaf in leaves:
            bgprib = [row['Network'] for row in fabric_routes['answerElements'][0]['rows']
                                        if row['Node']['name'] == leaf]
            if dstIP in bgprib:
                print(colored(f"route from {leaf} to {dstIP} present", 'green'))
            else:
                isFailed = True
                print(colored(f"{leaf} bgp rib doesn't include {dstIP} from leaf {dstNode}", 'red'))
                print(leaf, bgprib)
    return isFailed

load_questions()

bf_init_snapshot("./snapshot")

assert not test_controlplane(False), "Control Plane Checks Failed"
assert not check_routes(False, 'test-c99-f0103-lyc01a'), "Routing Checks Failed"

